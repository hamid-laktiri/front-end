export default class Produit{
    constructor(ref,nom,prix,origine){
        this.ref = ref
        this.nom = nom
        this.prix = prix
        this.origine = origine
    }
    info(){
        return ` ref = ${this.ref} nom = ${this.nom} prix=${this.prix} origine = ${this.origine}`
    }
}
export const products = []
